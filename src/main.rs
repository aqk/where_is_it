// Set tabwidth to 4
extern crate iron;
extern crate router;
extern crate rustc_serialize;

use iron::prelude::*;
use iron::status; 
use router::Router; 
use rustc_serialize::json;
use std::io::Read;

#[derive(RustcEncodable, RustcDecodable)]
struct Greeting {
    msg: String
}

fn main() {
    let mut router = Router::new();
	router.get("/", hello_world, "hello_world_get_greeting");
	router.post("/set", set_greeting, "hello_world_post_greeting");

    fn hello_world(_: &mut Request) -> IronResult<Response> {

    	let greeting = Greeting { msg: "Hello".to_string() };
		let payload = json::encode(&greeting);
    	Ok(Response::with((status::Ok, payload.unwrap())))
    }

    fn set_greeting(request: &mut Request) -> IronResult<Response> {
    	let mut payload = String::new();
    	request.body.read_to_string(&mut payload).unwrap();
    	let request: Greeting = json::decode(&mut payload).unwrap();
    	let greeting = Greeting { msg: request.msg };
		let payload = json::encode(&greeting).unwrap();
        Ok(Response::with((status::Ok, payload)))
	}

    println!("On port 4000");
    Iron::new(router).http("localhost:4000").unwrap();
}
